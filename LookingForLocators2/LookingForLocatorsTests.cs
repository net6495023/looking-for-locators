global using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace LookingForLocators2
{
    public class LookingForLocatorsTests
    {
        public IWebDriver Driver;

        [SetUp]
        public void Setup()
        {
            Driver = new ChromeDriver();
            Driver.Navigate().GoToUrl("https://demowf.aspnetawesome.com/");
            Driver.Manage().Window.Maximize();
        }

        [Test]
        public void LookingForLocators_FindElements_LocatorsFound()
        {            
            Driver.FindElement(By.ClassName("logo"));

            Driver.FindElement(By.Id("ContentPlaceHolder1_Meal"));

            Driver.FindElement(By.LinkText("Combobox"));

            Driver.FindElement(By.XPath("//label[@class='awe-label']/../../parent::div[@class='awe-display o-rdl']/.."));

            Driver.FindElement(By.XPath("//button[text()='Legumes']")).Click();

            Driver.FindElement(By.XPath("//input[@name='ctl00$ContentPlaceHolder1$ChildMeal1']/following-sibling::div[text()='Cauliflower']"));
            Driver.FindElement(By.XPath("//input[@name='ctl00$ContentPlaceHolder1$ChildMeal1']/following-sibling::div[text()='Lettuce']"));

            IWebElement comboControl = Driver.FindElement(By.XPath("//input[@id='ContentPlaceHolder1_AllMealsCombo-awed']"));
            comboControl.Clear();
            comboControl.SendKeys("Almond");
            Driver.FindElement(By.XPath("//div[@id='ContentPlaceHolder1_AllMealsCombo-dropmenu']//li[text()='Almond']")).Click();

            Assert.Pass();
        }

        [TearDown]
        public void Quit()
        {
            Driver.Quit();
        }
    }
}